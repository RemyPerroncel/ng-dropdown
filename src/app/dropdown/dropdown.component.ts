import { Component, OnInit } from '@angular/core';
import { Technicien } from './technicien.model';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  public technicienSelect: Technicien = undefined;
  public techniciens: Technicien[] = [];
  public isOpen = false;

  constructor() {
  }

  ngOnInit(): void {
    for (let i = 0; i < 10; i++) {
      this.techniciens.push(new Technicien('Tech ' + i, this.getRandomColor(), i > 1));
    }
  }

  public selectValue(value: Technicien): void {
    if (value.isPresent) {
      this.technicienSelect = value;
    }
  }

  public setOpen(): void {
    this.isOpen = !this.isOpen;
  }

  public getTechnicienColor(technicien: Technicien): {} {
    return {
      'background-color': technicien ? technicien.color : ''
    };
  }

  public getTechnicienStyle(technicien: Technicien): {} {
    return {
      color: technicien ? technicien.color : '',
      opacity: technicien && technicien.isPresent ? '1' : '0.1',
      background: technicien && technicien.isPresent ? '' : '#FFFFFF',
      cursor: technicien && technicien.isPresent ? 'pointer' : 'not-allowed'
    };
  }

  private getRandomColor(): string {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

}
