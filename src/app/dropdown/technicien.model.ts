export class Technicien {
  public name: string;
  public color: string;
  public isPresent: boolean;

  constructor(name: string, color: string, present: boolean) {
    this.name = name;
    this.color = color;
    this.isPresent = present;
  }
}
