import { AfterViewInit, Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appDropDown]'
})
export class DropdownDirective implements AfterViewInit {

  private static readonly DROPDOWN_HEIGHT = 250;
  private static readonly DROPDOWN_MARGIN = 15;
  private docViewTop: number;
  private docViewBottom: number;

  @Input()
  public set setIsOpen(open: boolean) {
    open ? this.checkElementPosition() : this.cleanClass();
  }

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

  public ngAfterViewInit(): void {
    this.docViewTop = window.pageYOffset;
    this.docViewBottom = this.docViewTop + window.innerHeight;
  }

  private checkElementPosition(): void {
    this.docViewTop = window.pageYOffset;
    this.docViewBottom = this.docViewTop + window.innerHeight;
    const elementPosition = this.el.nativeElement.offsetTop + this.el.nativeElement.offsetHeight +
      DropdownDirective.DROPDOWN_HEIGHT + DropdownDirective.DROPDOWN_MARGIN;
    if (elementPosition > this.docViewBottom) {
      this.renderer.addClass(this.el.nativeElement, 'drop-container--list-top');
    } else {
      this.renderer.addClass(this.el.nativeElement, 'drop-container--list-bot');
    }
  }

  private cleanClass(): void {
    this.renderer.removeClass(this.el.nativeElement, 'drop-container--list-top');
    this.renderer.removeClass(this.el.nativeElement, 'drop-container--list-bot');
  }
}
